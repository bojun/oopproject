
public class Player {
	public static final int NumOfPlayerCards = 16;
	Card[] hand;
	Card[] flowers;
	Card[] shown;
	int money;
	static int Dealer;
	boolean hued;
	boolean readyHand;

	static {
		Dealer = (int) (Math.random() * 4);
	}

	/**
	 * Default constructor set money to 10000.
	 */

	public Player() {
		this.hand = new Card[NumOfPlayerCards];
		this.flowers = new Card[0];
		this.shown = new Card[0];
		this.money = 10000;
		readyHand = false;
	}

	/**
	 * Get cards in this player's hand.
	 * 
	 * @return Card[] the cards in this player's hand
	 */

	public Card[] getHand() {
		return hand;
	}

	/**
	 * Set cards in this player's hand.
	 * 
	 * @param hand
	 *            a card array to be set as this player's hand
	 */

	public void setHand(Card[] hand) {
		this.hand = hand;
	}

	/**
	 * Get flowers of this player.
	 * 
	 * @return Card[] flowers of this player
	 */

	public Card[] getFlowers() {
		return flowers;
	}

	/**
	 * Set flowers of this player
	 * 
	 * @param flowers
	 *            a card array to be set as the flowers of this player
	 */
	public void setFlowers(Card[] flowers) {
		this.flowers = flowers;
	}

	/**
	 * players[getDealer()] is the dealer
	 * 
	 * @return int the dealer
	 */
	public static int getDealer() {
		return Dealer;
	}

	/**
	 * Set Dealer to input integer
	 * 
	 * @param dealer
	 *            Dealer
	 */
	public static void setDealer(int dealer) {
		Dealer = dealer;
	}

	/**
	 * Get the amount of money this player has
	 * 
	 * @return int the amount of money this player has
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * Set the amount of money this player has
	 * 
	 * @param money
	 *            the amount of money this player has
	 */
	public void setMoney(int money) {
		this.money = money;
	}

	/**
	 * Return a boolean[] has same length as this player's hand. boolean[i] is
	 * true if hand[i] is flower, false if not.
	 * 
	 * @return boolean[] tells whether the cards in this player's hand is flower
	 */
	public boolean[] flower() {
		boolean[] boo = new boolean[hand.length];
		for (int i = 0; i < hand.length; i++) {
			boo[i] = hand[i].isFlower();
		}
		return boo;
	}

	/**
	 * Add a card to flower
	 * 
	 * @param card
	 *            a flower
	 */

	public void addFlowers(Card card) {
		Card[] flr = getFlowers();
		Card[] temp = new Card[flr.length + 1];
		for (int i = 0; i < flr.length; i++) {
			temp[i] = flr[i];
		}
		temp[flr.length] = card;
		this.setFlowers(temp);

	}

	/**
	 * Set hand[j] to aCard
	 * 
	 * @param card
	 *            a Card to be set as hand[j]
	 * @param j
	 *            the position in hand to be changed
	 */

	public void setHand(Card card, int j) {
		hand[j] = card;
	}

	/**
	 * Return true whether this player hued, false if not.
	 * 
	 * @return boolean whether this player hued
	 */
	public boolean isHued() {
		return hued;
	}

	/**
	 * Set hued of this player.
	 */
	public void setHued(boolean hued) {
		this.hued = hued;
	}

	/**
	 * Return true if hand is hu, false if not.
	 * 
	 * @return boolean whether this player's hand is huable
	 */
	public boolean huable() {
		System.out.println("Player.huable() isn't finished yet.");
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Add a card to hand
	 * 
	 * @param card
	 *            a card to be added to hand
	 */

	public void addHand(Card card) {
		Card[] temp = new Card[hand.length + 1];
		for (int i = 0; i < hand.length; i++) {
			temp[i] = hand[i];
		}
		temp[hand.length] = card;
		hand = temp;

	}

	/**
	 * Sort hand to increment by card's name.
	 */
	public void sortHand() {
		System.out.println("Player.sortHand() isn't finished yet.");
		// TODO Auto-generated method stub

	}

	/**
	 * Tells this player what's in his/her hand and ask this player to play a
	 * card.
	 */

	public void playACard() {
		System.out.println("Player.playACard() isn't finished yet.");
		// TODO Auto-generated method stub

	}

	/**
	 * Return true if hand is readable, false if not.
	 * 
	 * @return boolean whether this player's hand is readable
	 */
	public boolean readable() {
		System.out.println("Player.readable() isn't finished yet.");
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Set readyHand to the boolean
	 * 
	 * @param bool
	 *            a boolean to be set as readyHand
	 */
	public void setReadyHand(boolean bool) {
		readyHand = bool;

	}

}