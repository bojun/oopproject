
public class Deck {
	private static final int NumOfCards = 144;
	Card[] deck = new Card[NumOfCards];

	/**
	 * Default Constructor creat a random deck
	 */
	public Deck() {
		Card[] temp = new Card[NumOfCards];
		for (int i = 0; i < NumOfCards; i++) {
			if (i < 136) {
				temp[i] = new Card(i / 4);
			}
			else{
				temp[i] = new Card(i - 102);
			}
		}
		Card[] temp2 = null;
		int ran;
		for (int i = 0; i < NumOfCards; i++) {
			ran = (int) (Math.random() * (temp.length));
			deck[i] = temp[ran];
			temp2 = new Card[temp.length - 1];
			for (int j = ran; j < temp2.length; j++) {
				temp2[j] = temp[j + 1];
			}
			for (int k = 0; k < ran; k++) {
				temp2[k] = temp[k];
			}
			temp = temp2;
		}
	}

	/**
	 * Get a card and delete it from deck
	 * 
	 * @return Card
	 */
	public Card getACard() {
		int len = deck.length;
		Card temp = deck[len - 1];
		Card[] temp2 = new Card[deck.length - 1];
		for (int i = 0; i < deck.length - 1; i++) {
			temp2[i] = deck[i];
		}
		deck = temp2;
		return temp;
	}

	public static void main(String[] args) {

	}
}
