
import java.util.Scanner;

public class PlayMahjong {

	private static final int NumOfPlayerCards = 16;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Player[] players = new Player[4];
		for (int i = 0; i < 4; i++) {
			players[i] = new Player();
		}
		Deck remain = new Deck();

		for (int j = 0; j < 4; j++) { // 發牌
			Card[] temp = new Card[NumOfPlayerCards];
			for (int i = 0; i < NumOfPlayerCards; i++) {
				temp[i] = remain.getACard();
			}
			players[j].setHand(temp);
		}

		boolean hued = false;// temp
		for (int i = 0; i < 4; i++) { // 補花
			boolean[] boo = players[i].flower();

			for (int j = 0; j < boo.length; j++) {

				if (boo[j]) {

					players[i].addFlowers(players[i].getHand()[j]);
					players[i].setHand(remain.getACard(), j);

				}
			}
		}
		int dealer = Player.getDealer(); // 莊家
		System.out.println("Player " + dealer + " is the dealer!");
		int now = dealer;
		int nextPlayer = now;
		boolean play = true;
		while (play) {

			while (!hued) {
				now = nextPlayer;
				Card tempCard = remain.getACard();
				for (; tempCard.isFlower();) {
					players[now].addFlowers(tempCard);
					tempCard = remain.getACard();
				}
				players[now].addHand(tempCard);
				players[now].sortHand();
				if (players[now].huable()) {
					System.out.print("是否要胡牌?(true/false)");
					boolean booIn = scanner.nextBoolean();
					if (booIn) {
						players[now].setHued(true);
						hued = true;
					} else {
						players[now].playACard();
					}
				} else if (players[now].readable()) {
					System.out.print("是否要聽牌?(true/false)");
					boolean booIn = scanner.nextBoolean();
					if (booIn) {
						players[now].setReadyHand(true);
					}
					players[now].playACard();
				} else {
					players[now].playACard();
				}
				nextPlayer = now + 1 > 3 ? now - 3 : now + 1;
				System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nPlayer "
						+ nextPlayer + " ready?(true/false)\n");
				boolean booIn = scanner.nextBoolean();
				while (!booIn) {
					System.out.print("Player " + nextPlayer + " ready?(true/false)");
					booIn = scanner.nextBoolean();
				}
			}
			
			// 給錢
			System.out.print("Keep playing?(true/false)");
			boolean booIn = scanner.nextBoolean();
			play = booIn;
		}

	}

}
