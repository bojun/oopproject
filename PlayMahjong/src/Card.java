
public class Card {
	/**
	 * 0 to 41, same sequence as unicode's. more information:
	 * http://www.unicode.org/charts/PDF/U1F000.pdf
	 */
	int name;

	/**
	 * Constructor set name
	 * 
	 * @param name
	 *            set this card's as name
	 */

	public Card(int name) {
		this.name = name;
	}

	/**
	 * Get name
	 * 
	 * @return int the name of the card
	 */

	public int getName() {
		return name;
	}

	/**
	 * Set this card's name as input int
	 * 
	 * @param name
	 *            set this card's name as the int
	 */

	public void setName(int name) {
		this.name = name;
	}

	/**
	 * isFlower() returns true if this card is flower, false if not.
	 * 
	 * @return boolean whether this card is flower
	 */
	public boolean isFlower() {
		return 34 <= name;
	}

}
