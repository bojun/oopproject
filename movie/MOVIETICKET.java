/**
 * The class
 * 
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
public class MOVIETICKET {
	public int MANID;
	public int TICKETID;
	public String NAME;
	public String TIME;
	public String HALL;
	public String SEAT;
	public String[] row;
	public int[] column;
	public int num;

	/**
	 * The default constructor of MOVIETICKET
	 */
	public MOVIETICKET() {
		this.MANID = 0;
		this.TICKETID = 0;
		this.NAME = new String();
		this.TIME = new String();
		this.HALL = new String();
		this.SEAT = new String();
	}

	/**
	 * The constructor
	 * 
	 * @param MANID
	 *            The index of the person
	 * @param TICKETID
	 *            The ID of the ticket
	 * @param NAME
	 *            The movie name
	 * @param TIME
	 *            The movie's playing time
	 * @param HALL
	 *            The playing room
	 * @param SEAT
	 *            The user's seat
	 */
	public MOVIETICKET(int MANID, int TICKETID, String NAME, String TIME, String HALL, String SEAT, String[] row,
			int[] column, int num) {
		this.MANID = MANID;
		this.TICKETID = TICKETID;
		this.NAME = NAME;
		this.TIME = TIME;
		this.HALL = HALL;
		this.SEAT = SEAT;
		this.row = row;
		this.column = column;
		this.num = num;
	}

	// SETTER
	/**
	 * The method sets the index of the person
	 * 
	 * @param MANID
	 *            Index of the person
	 */
	public void setMANID(int MANID) {
		this.MANID = MANID;
	}

	/**
	 * The method sets the ID of the ticket
	 * 
	 * @param MANID
	 *            The ID of the ticket
	 */
	public void setTICKETID(int TICKETID) {
		this.TICKETID = TICKETID;
	}

	/**
	 * The method sets the name of the movie
	 * 
	 * @param NAME
	 *            The movie name
	 */
	public void setNAME(String NAME) {
		this.NAME = NAME;
	}

	/**
	 * The method sets the movie's playing time
	 * 
	 * @param TIME
	 *            The movie's playing time
	 */
	public void setTIME(String TIME) {
		this.TIME = TIME;
	}

	/**
	 * The method sets the movie's playing room
	 * 
	 * @param HALL
	 *            The movie's playing room
	 */
	public void setHALL(String HALL) {
		this.HALL = HALL;
	}

	/**
	 * The method sets the user's seat
	 * 
	 * @param SEAT
	 *            The user's seat
	 */
	public void setSEAT(String SEAT) {
		this.SEAT = SEAT;
	}

	// GETTER
	/**
	 * The method gets the index of the person
	 * 
	 * @return Index of the person
	 */
	public int getMANID() {
		return MANID;
	}

	/**
	 * The method gets the ID of the ticket
	 * 
	 * @return The ID of the ticket
	 */
	public int getTICKETID() {
		return TICKETID;
	}

	/**
	 * The method gets the name of the movie
	 * 
	 * @return The movie name
	 */
	public String getNAME() {
		return NAME;
	}

	/**
	 * The method gets the movie's playing time
	 * 
	 * @return The movie's playing time
	 */
	public String getTIME() {
		return TIME;
	}

	/**
	 * The method gets the movie's playing room
	 * 
	 * @return The movie's playing room
	 */
	public String getHALL() {
		return HALL;
	}

	/**
	 * The method gets the user's seat
	 * 
	 * @return The user's seat
	 */
	public String getSEAT() {
		return SEAT;
	}
	/**
	 * The method gets the row
	 * 
	 * @return The row
	 */
	public String[] getrow() {
		return row;
	}
	/**
	 * The method gets the column
	 * 
	 * @return The column
	 */
	public int[] getcolumn() {
		return column;
	}
	/**
	 * The method gets the number
	 * 
	 * @return The number
	 */
	public int getnum() {
		return num;
	}

}
