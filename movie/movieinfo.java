/**
 * The class includes movie information.We can some methods to check the movie information
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
public class movieinfo {
	public String id;
	public String movie;
	public String url;
	public int classification;
	public String classification2;
	public String descri;
	public String infor;
	public String score;
	public double score2;
	public String time;
	public String[] time2;
	public String hall;
	public boolean judge = false;
	/**
	 *The default constructor 
	 */
	public movieinfo(){
		this.id = new String();
		this.movie = new String();
		this.url = new String();
		this.classification = 0;
		this.classification2 = new String();
		this.descri = new String();
		this.infor = new String();
		this.score = new String();
		this.score2 = 0;
		this.time = new String();
		this.time2 = new String[0];
		this.hall = new String();
	}
	/**
	 * The constructor that provides the information of the movie
	 * @param id The movie ID
	 */
	public movieinfo(String id){
		this.id = new String();
		this.movie = new String();
		this.url = new String();
		this.classification = 0;
		this.classification2 = new String();
		this.descri = new String();
		this.infor = new String();
		this.score = new String();
		this.score2 = 0;
		this.time = new String();
		this.time2 = new String[0];
		this.hall = new String();
		switch(id){
		case "3R47wXhjjLqnLWef6HU155ek":
			this.id = "3R47wXhjjLqnLWef6HU155ek";
			this.movie = " 異形：聖約 Alien: Covenant ";
			this.url = "http://www.atmovies.com.tw/movie/faen42316204/";
			this.classification = 15;
			this.classification2 = "輔導級";
			this.descri = "好萊塢傳奇大導雷利史考特重返經典科幻鉅作，與《異形》故事連結，並作為以《普羅米修斯》為首的前傳三部曲之第二章節。殖民太空船—聖約號發現了一個未知的世界，由仿生人—大衛所主宰。";
		    this.infor = " 片長：122分 ";
		    this.score = " 7.7/53 ";
		    this.score2 = 7.7;
		    this.time = "09：40、12：00、14：20、16：40、19：00、21：20、23：40";
		    this.time2 =time.split("、");
		    this.hall = "武當";
		    this.judge = true;
		    break;
		case "H55WYTppJHIwZyCfUHJLIbWC":
			this.id = "H55WYTppJHIwZyCfUHJLIbWC";
			this.movie = " 亞瑟：王者之劍 King Arthur: Legend of the Sword ";
			this.url = "http://www.atmovies.com.tw/movie/fken33496992/";
			this.classification = 6;
			this.classification2 = "保護級";
			this.descri = "蓋瑞奇執導，《環太平洋》查理漢納、裘德洛、艾瑞克巴納等。電影重新詮釋「亞瑟王」與「石中劍」這個流傳千年的傳奇故事，用嶄新的觀點敘述這段膾炙人口的史詩。看亞瑟如何從一無所有的街頭少年，成為偉大的王者。";
		    this.infor = " 片長：127分 ";
		    this.score = " 8.5/47 ";
		    this.score2 = 8.5;
		    this.time = "09：05、11：30、14：45、17：10、19：35、22：05";
		    this.time2 = time.split("、");
		    this.hall = "少林";
		    this.judge = true;
		    break;
		case "Q9xwOoSHCD0Xwfm2EiDGRn3Z":
			this.id = "Q9xwOoSHCD0Xwfm2EiDGRn3Z";
			this.movie = " 逃出尼哥鎮 Get Out of the nigger town ";
			this.url = "http://www.atmovies.com.tw/movie/fgen55052448/";
			this.classification = 18;
			this.classification2 = "限制級";
			this.descri = "阿奇與阿皮》喬登皮爾首部執導，斯史坦費爾德、凱薩琳凱娜主演。一名年輕非裔男生到他的白人女朋友的莊園家中作客，沒想到遇上讓他永生難忘的種族夢靨。";
		    this.infor = " 片長：104分 ";
		    this.score = " 7.8/76 ";
		    this.score2 = 7.8;
		    this.time = "11：20、15：20、19：10、23：00";
		    this.time2 = time.split("、");
		    this.hall = "華山";
		    this.judge = true;
		    break;
		case "WGd6f01Om27eSmo9X3b6cuXu":
			this.id = "WGd6f01Om27eSmo9X3b6cuXu";
			this.movie = " 電影版影子籃球員LAST GAME ";
			this.url = "http://www.atmovies.com.tw/movie/flja93513883/";
			this.classification = 0;
			this.classification2 = "普遍級";
			this.descri = "原作漫畫由藤卷忠俊所著，充滿感動與熱血的籃球作品《影子籃球員》，2017年將推出全新的電影。劇情延續動畫本篇，火神與黑子二人升上了二年級後的夏末，將與「奇蹟世代」的成員組隊，迎擊美國街球隊伍。";
		    this.infor = " 片長：91分 ";
		    this.score = " 4.3/78 ";
		    this.score2 = 4.3;
		    this.time = "09：10、11：15、13：00";
		    this.time2 = time.split("、");
		    this.hall = "峨嵋";
		    this.judge = true;
		    break;
		case "pYXEwQEWFBBFarOD5LBJmnTU":
			this.id = "pYXEwQEWFBBFarOD5LBJmnTU";
			this.movie = " 攻殼機動隊1995 GHOST IN THE SHELL ";
			this.url = "http://www.atmovies.com.tw/movie/fgjp50113568/";
			this.classification = 6;
			this.classification2 = "保護級";
			this.descri = "動畫鬼才押井守操刀，攻殼機動隊系列的第一部動畫電影回歸大銀幕。劇情大致來自士郎正宗之原著漫畫《攻殼機動隊》，「公安九課」草薙素子率領其他隊員追查未來世界裡神祕的駭客「傀儡師」。";
		    this.infor = " 片長：83分 ";
		    this.score = " 7.0/23 ";
		    this.score2 = 7.0;
		    this.time = "17：50、21：30";
		    this.time2 = time.split("、");
		    this.hall = "峨嵋";
		    this.judge = true;
		    break;
		case "x7GCv22RgYGfd5l8YdbVqYhZ":
			this.id = "x7GCv22RgYGfd5l8YdbVqYhZ";
			this.movie = " 我和我的冠軍女兒 Dangal ";
			this.url = "http://www.atmovies.com.tw/movie/fdin65074352/";
			this.classification = 0;
			this.classification2 = "普遍級";
			this.descri = "「印度3K天王」阿米爾罕再度自製自演的最新電影，改編自印度摔跤選手瑪哈維亞的傳奇感人故事、不可思議的真人真事，父女情深聯手挑戰印度社會性別疆界。摔跤不是男生的事，我們家的女孩不會輸！？";
		    this.infor = " 片長：161分 ";
		    this.score = " 9.4/307 ";
		    this.score2 = 9.4;
		    this.time = "10：50、13：50、16：50、19：50、21：50";
		    this.time2 = time.split("、");
		    this.hall = "崆峒";
		    this.judge = true;
		    break;
		default:
			System.out.println("失敗，此電影ID不存在。");
			this.judge = false;
			break;		
		}
	}
	/**
	 * The method prints the movies that fits the input rank
	 * @param rank the input movie rank
	 */
	public static void movierank(double rank){
		String result;
		if (rank <= 9.4 && rank > 8.5){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ";
		}
		else if(rank <= 8.5 && rank > 7.8){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ，H55WYTppJHIwZyCfUHJLIbWC";
		}
		else if(rank <= 7.8 && rank > 7.7){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ，H55WYTppJHIwZyCfUHJLIbWC，Q9xwOoSHCD0Xwfm2EiDGRn3Z";
		}
		else if(rank <= 7.7 && rank > 7.0){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ，H55WYTppJHIwZyCfUHJLIbWC，Q9xwOoSHCD0Xwfm2EiDGRn3Z，3R47wXhjjLqnLWef6HU155ek";
		}
		else if(rank <= 7.0 && rank > 4.3){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ，H55WYTppJHIwZyCfUHJLIbWC，Q9xwOoSHCD0Xwfm2EiDGRn3Z，3R47wXhjjLqnLWef6HU155ek，pYXEwQEWFBBFarOD5LBJmnTU";
		}
		else if(rank <= 4.3 && rank >= 0){
			result = "x7GCv22RgYGfd5l8YdbVqYhZ，H55WYTppJHIwZyCfUHJLIbWC，Q9xwOoSHCD0Xwfm2EiDGRn3Z，3R47wXhjjLqnLWef6HU155ek，pYXEwQEWFBBFarOD5LBJmnTU，WGd6f01Om27eSmo9X3b6cuXu";
		}
		else if(rank <= 10.0 && rank > 9.4){
			result = "no movie rated higher than " + rank;
		}
		else{
			result = "error, the score should set between 0 to 10.";
		}
		System.out.println(result);
	}
	/**
	 * The method  prints the movie information which we input
	 * @param movie the input movie name
	 */
	public static void movieinfo(String movie){
		switch (movie){
		case "3R47wXhjjLqnLWef6HU155ek":
			System.out.println("電影名稱：異形：聖約 Alien: Covenant");
			System.out.println("分級：輔導級");
			System.out.println("放映時間：<09：40>、<12：00>、<14：20>、<16：40>、<19：00>、<21：20>、<23：40>");
			System.out.println("廳位：武當");
			break;
		case "H55WYTppJHIwZyCfUHJLIbWC":
			System.out.println("電影名稱：亞瑟：王者之劍 King Arthur: Legend of the Sword");
			System.out.println("分級：保護級");
			System.out.println("放映時間：<09：05>、<11：30>、<14：45>、<17：10>、<19：35>、<22：05>");
			System.out.println("廳位：少林");
			break;
		case "Q9xwOoSHCD0Xwfm2EiDGRn3Z":
			System.out.println("電影名稱：逃出尼哥鎮 Get Out of the nigger town");
			System.out.println("分級：限制級");
			System.out.println("放映時間：<11：20>、<15：20>、<19：10>、<23：00>");
			System.out.println("廳位：華山");
			break;
		case "WGd6f01Om27eSmo9X3b6cuXu":
			System.out.println("電影名稱：電影版影子籃球員LAST GAME");
			System.out.println("分級：普遍級");
			System.out.println("放映時間：<09：10>、<11：15>、<13：00>");
			System.out.println("廳位：峨嵋");
			break;
		case "pYXEwQEWFBBFarOD5LBJmnTU":
			System.out.println("電影名稱：攻殼機動隊1995 GHOST IN THE SHELL");
			System.out.println("分級：保護級");
			System.out.println("放映時間：<17:50>、<21:30>");
			System.out.println("廳位：峨嵋");
			break;
		case "x7GCv22RgYGfd5l8YdbVqYhZ":
			System.out.println("電影名稱：我和我的冠軍女兒 Dangal");
			System.out.println("分級：普遍級");
			System.out.println("放映時間：<10：50>、<13：50>、<16：50>、<19：50>、<21：50>");
			System.out.println("廳位：崆峒");
			break;
		default:
			System.out.println("error，the ID you input isn't exist.");
		}	
	}
	
	

}