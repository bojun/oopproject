/**
 * The bigroom class provides the big movie room's information and some methods
 * of it.
 * 
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
public class bigroom {
	public String time;
	public String name;
	public String id;
	public int playtime;
	public int bigseatnum;
	public int[] column;
	public String cond1 = "無特殊條件";
	public String cond2 = "同排";
	public String[] A;
	public String[] B;
	public String[] C;
	public String[] D;
	public String[] E;
	public String[] F;
	public String[] G;
	public String[] H;
	public String[] I;
	public String[] J;
	public String[] K;
	public String[] L;
	public String[] M;
	public int total = 0, totalA = 0, totalB = 0, totalC = 0, totalD = 0;
	public int totalE = 0, totalF = 0, totalG = 0, totalH = 0, totalI = 0;
	public int totalJ = 0, totalK = 0, totalL = 0, totalM = 0;
	public String SEAT = new String();
	public String[] row;
	public int[] column2;
	public int num = 0;

	/**
	 * default constructor
	 */
	public bigroom() {
		this.time = new String();
		this.name = new String();
		this.id = new String();
		this.playtime = 0;
		this.bigseatnum = 0;
		column = new int[39];
		A = new String[38];
		B = new String[38];
		C = new String[38];
		D = new String[38];
		E = new String[38];
		F = new String[38];
		G = new String[38];
		H = new String[38];
		I = new String[38];
		J = new String[38];
		K = new String[38];
		L = new String[39];
		M = new String[38];
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The movie room name
	 * @param time
	 *            The movie length
	 */
	public bigroom(String name, String time) {
		this.time = time;
		switch (name) {
		case "武當":
			this.name = name;
			this.id = "3R47wXhjjLqnLWef6HU155ek";
			this.playtime = 122;
			this.bigseatnum = 0;
			column = new int[39];
			A = new String[38];
			B = new String[38];
			C = new String[38];
			D = new String[38];
			E = new String[38];
			F = new String[38];
			G = new String[38];
			H = new String[38];
			I = new String[38];
			J = new String[38];
			K = new String[38];
			L = new String[39];
			M = new String[38];
			setbigSEAT();
			break;
		case "少林":
			this.name = name;
			this.id = "H55WYTppJHIwZyCfUHJLIbWC";
			this.playtime = 127;
			this.bigseatnum = 0;
			column = new int[39];
			A = new String[38];
			B = new String[38];
			C = new String[38];
			D = new String[38];
			E = new String[38];
			F = new String[38];
			G = new String[38];
			H = new String[38];
			I = new String[38];
			J = new String[38];
			K = new String[38];
			L = new String[39];
			M = new String[38];
			setbigSEAT();
			break;
		case "華山":
			this.name = name;
			this.id = "Q9xwOoSHCD0Xwfm2EiDGRn3Z";
			this.playtime = 104;
			this.bigseatnum = 0;
			column = new int[39];
			A = new String[38];
			B = new String[38];
			C = new String[38];
			D = new String[38];
			E = new String[38];
			F = new String[38];
			G = new String[38];
			H = new String[38];
			I = new String[38];
			J = new String[38];
			K = new String[38];
			L = new String[39];
			M = new String[38];
			setbigSEAT();
			break;
		default:
			System.out.println("沒有這個影廳");
			break;
		}

	}

	/**
	 * The method can check if the room has enough seats.
	 * 
	 * @param seat
	 *            The number of seats
	 * @param maxtime
	 *            The maximum time
	 * @param mintime
	 *            The minimum time
	 * @return true or false
	 */
	public boolean conditionOK(int seat, int maxtime, int mintime) {
		return this.bigseatnum() >= seat && this.playtime <= maxtime && this.playtime >= mintime;
	}

	/**
	 * The method creates the seating chart of the room
	 */
	public void setbigSEAT() {
		// set column
		for (int i = 0; i < column.length; i++) {
			column[i] = i + 1;
		}
		// set row A
		for (int i = 0; i < 7; i++) {
			A[i] = null + " ";
		}
		for (int i = 7; i < 11; i++) {
			A[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			A[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			A[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			A[i] = null + " ";
		}
		for (int i = 27; i < 31; i++) {
			A[i] = "O    ";
		}
		for (int i = 31; i < 38; i++) {
			A[i] = null + " ";
		}
		// set row B
		for (int i = 0; i < 4; i++) {
			B[i] = null + " ";
		}
		for (int i = 4; i < 11; i++) {
			B[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			B[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			B[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			B[i] = null + " ";
		}
		for (int i = 27; i < 34; i++) {
			B[i] = "O    ";
		}
		for (int i = 34; i < 38; i++) {
			B[i] = null + " ";
		}
		// set row C
		for (int i = 0; i < 11; i++) {
			C[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			C[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			C[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			C[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			C[i] = "O    ";
		}
		// set row D
		for (int i = 0; i < 11; i++) {
			D[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			D[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			D[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			D[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			D[i] = "O    ";
		}
		// set row E
		for (int i = 0; i < 11; i++) {
			E[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			E[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			E[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			E[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			E[i] = "O    ";
		}
		// set row F
		for (int i = 0; i < 11; i++) {
			F[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			F[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			F[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			F[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			F[i] = "O    ";
		}
		// set row G
		for (int i = 0; i < 11; i++) {
			G[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			G[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			G[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			G[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			G[i] = "O    ";
		}
		// set row H
		for (int i = 0; i < 11; i++) {
			H[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			H[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			H[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			H[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			H[i] = "O    ";
		}
		// set row I
		for (int i = 0; i < 11; i++) {
			I[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			I[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			I[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			I[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			I[i] = "O    ";
		}
		// set row J
		for (int i = 0; i < 11; i++) {
			J[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			J[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			J[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			J[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			J[i] = "O    ";
		}
		// set row K
		for (int i = 0; i < 11; i++) {
			K[i] = "O    ";
		}
		for (int i = 11; i < 13; i++) {
			K[i] = null + " ";
		}
		for (int i = 13; i < 25; i++) {
			K[i] = "O    ";
		}
		for (int i = 25; i < 27; i++) {
			K[i] = null + " ";
		}
		for (int i = 27; i < 38; i++) {
			K[i] = "O    ";
		}
		// set row L
		for (int i = 0; i < L.length; i++) {
			L[i] = "O    ";
		}
		// set row M
		for (int i = 0; i < 7; i++) {
			M[i] = "O    ";
		}
		for (int i = 7; i < 30; i++) {
			M[i] = null + " ";
		}
		for (int i = 30; i < 38; i++) {
			M[i] = "O    ";
		}
	}

	/**
	 * The method calculates the remain numbers of seats of the big movie room.
	 * 
	 * @return the remain numbers of seats of the big movie room.
	 */
	public int bigseatnum() {
		// 計算大電影廳的剩餘座位數量
		for (int i = 0; i < A.length; i++) {
			if (A[i].equals("O    ")) {
				totalA = totalA + 1;
			}
		}
		for (int i = 0; i < B.length; i++) {
			if (B[i].equals("O    ")) {
				totalB = totalB + 1;
			}
		}
		for (int i = 0; i < C.length; i++) {
			if (C[i].equals("O    ")) {
				totalC = totalC + 1;
			}
		}
		for (int i = 0; i < D.length; i++) {
			if (D[i].equals("O    ")) {
				totalD = totalD + 1;
			}
		}
		for (int i = 0; i < E.length; i++) {
			if (E[i].equals("O    ")) {
				totalE = totalE + 1;
			}
		}
		for (int i = 0; i < F.length; i++) {
			if (F[i].equals("O    ")) {
				totalF = totalF + 1;
			}
		}
		for (int i = 0; i < G.length; i++) {
			if (G[i].equals("O    ")) {
				totalG = totalG + 1;
			}
		}
		for (int i = 0; i < H.length; i++) {
			if (H[i].equals("O    ")) {
				totalH = totalH + 1;
			}
		}
		for (int i = 0; i < I.length; i++) {
			if (I[i].equals("O    ")) {
				totalI = totalI + 1;
			}
		}
		for (int i = 0; i < J.length; i++) {
			if (J[i].equals("O    ")) {
				totalJ = totalJ + 1;
			}
		}
		for (int i = 0; i < K.length; i++) {
			if (K[i].equals("O    ")) {
				totalK = totalK + 1;
			}
		}
		for (int i = 0; i < L.length; i++) {
			if (L[i].equals("O    ")) {
				totalL = totalL + 1;
			}
		}
		for (int i = 0; i < M.length; i++) {
			if (M[i].equals("O    ")) {
				totalM = totalM + 1;
			}
		}
		total = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI + totalJ + totalK
				+ totalL + totalM;
		// System.out.println("現在總共有" + total + "個空位。");
		return total;
	}

	/**
	 * The method provides the special request booking 
	 * @param ticket the number of tickets
	 * @param cond1 have special request or not
	 * @param cond2 another request
	 * @param row the row which requested
	 * @return true or false whether the booking is successful
	 */
	public boolean specialseat(int ticket, String cond1, String cond2, String row) {
		boolean result = false;
		int EMPTYG = 0, EMPTYH = 0, EMPTYI = 0, EMPTYJ = 0, EMPTYK = 0, EMPTYL = 0, EMPTY = 0;
		switch (cond1) {
		case "無特殊條件":
			switch (cond2) {
			case "同排":
				switch (row) {
				case "A":
					if (ticket > totalA) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "B":
					if (ticket > totalB) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "C":
					if (ticket > totalC) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "D":
					if (ticket > totalD) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "E":
					if (ticket > totalE) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "F":
					if (ticket > totalF) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "G":
					if (ticket > totalG) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "H":
					if (ticket > totalH) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "I":
					if (ticket > totalI) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "J":
					if (ticket > totalJ) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "K":
					if (ticket > totalK) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "L":
					if (ticket > totalL) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				case "M":
					if (ticket > totalM) {
						System.out.println("no enough seats.");
						result = false;
					} else {
						result = true;
					}
					break;
				default:
					result = false;
					System.out.println("invalid row selected");
					break;
				}
				break;
			case "無特殊條件":
				if (total > ticket) {
					result = true;
				} else {
					System.out.println("no enough seats.");
					result = false;
				}
				break;
			default:
				result = false;
				System.out.println("輸入條件錯誤");
			}
			break;
		case "次佳位置":
			// G row
			for (int i = 7; i < 11; i++) {
				if (G[i] == "O    ") {
					EMPTYG = EMPTYG + 1;
				}
			}
			for (int i = 13; i < 25; i++) {
				if (G[i] == "O    ") {
					EMPTYG = EMPTYG + 1;
				}
			}
			for (int i = 27; i < 31; i++) {
				if (G[i] == "O    ") {
					EMPTYG = EMPTYG + 1;
				}
			}
			// H row
			for (int i = 7; i < 9; i++) {
				if (H[i] == "O    ") {
					EMPTYH = EMPTYH + 1;
				}
			}
			for (int i = 29; i < 31; i++) {
				if (H[i] == "O    ") {
					EMPTYH = EMPTYH + 1;
				}
			}
			// I row
			for (int i = 7; i < 9; i++) {
				if (I[i] == "O    ") {
					EMPTYI = EMPTYI + 1;
				}
			}
			for (int i = 29; i < 31; i++) {
				if (I[i] == "O    ") {
					EMPTYI = EMPTYI + 1;
				}
			}
			// J row
			for (int i = 7; i < 9; i++) {
				if (J[i] == "O    ") {
					EMPTYJ = EMPTYJ + 1;
				}
			}
			for (int i = 29; i < 31; i++) {
				if (J[i] == "O    ") {
					EMPTYJ = EMPTYJ + 1;
				}
			}
			// K row
			for (int i = 7; i < 9; i++) {
				if (K[i] == "O    ") {
					EMPTYK = EMPTYK + 1;
				}
			}
			for (int i = 29; i < 31; i++) {
				if (K[i] == "O    ") {
					EMPTYK = EMPTYK + 1;
				}
			}
			// L row
			for (int i = 5; i < 9; i++) {
				if (L[i] == "O    ") {
					EMPTYL = EMPTYL + 1;
				}
			}
			for (int i = 31; i < 35; i++) {
				if (L[i] == "O    ") {
					EMPTYL = EMPTYL + 1;
				}
			}
			EMPTY = EMPTYG + EMPTYH + EMPTYI + EMPTYJ + EMPTYK + EMPTYL;
			if (EMPTY >= ticket) {
				switch (cond2) {
				case "同排":
					if (row.equals("G")) {
						if (EMPTYG >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("H")) {
						if (EMPTYH >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("I")) {
						if (EMPTYI >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("J")) {
						if (EMPTYJ >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("K")) {
						if (EMPTYK >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("L")) {
						if (EMPTYL >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else {
						System.out.println("row " + row + " 沒有次佳位置");
						result = false;
					}
					break;
				case "不需同排":
					result = true;
					break;
				default:
					result = false;
					System.out.println("輸入條件錯誤。");
					break;
				}
			} else {
				System.out.println("次佳位置區域已經沒有足後的座位了。");
				result = false;
			}
			break;
		case "最佳位置":
			// H row
			for (int i = 9; i < 11; i++) {
				if (H[i] == "O    ") {
					EMPTYH = EMPTYH + 1;
				}
			}
			for (int i = 13; i < 25; i++) {
				if (H[i] == "O    ") {
					EMPTYH = EMPTYH + 1;
				}
			}
			for (int i = 27; i < 29; i++) {
				if (H[i] == "O    ") {
					EMPTYH = EMPTYH + 1;
				}
			}
			// I row
			for (int i = 9; i < 11; i++) {
				if (I[i] == "O    ") {
					EMPTYI = EMPTYI + 1;
				}
			}
			for (int i = 27; i < 29; i++) {
				if (I[i] == "O    ") {
					EMPTYI = EMPTYI + 1;
				}
			}
			// J row
			for (int i = 9; i < 11; i++) {
				if (J[i] == "O    ") {
					EMPTYJ = EMPTYJ + 1;
				}
			}
			for (int i = 27; i < 29; i++) {
				if (J[i] == "O    ") {
					EMPTYJ = EMPTYJ + 1;
				}
			}
			// K row
			for (int i = 9; i < 11; i++) {
				if (K[i] == "O    ") {
					EMPTYK = EMPTYK + 1;
				}
			}
			for (int i = 13; i < 25; i++) {
				if (K[i] == "O    ") {
					EMPTYK = EMPTYK + 1;
				}
			}
			for (int i = 27; i < 29; i++) {
				if (K[i] == "O    ") {
					EMPTYK = EMPTYK + 1;
				}
			}
			// L row
			for (int i = 9; i < 31; i++) {
				if (L[i] == "O    ") {
					EMPTYL = EMPTYL + 1;
				}
			}
			EMPTY = EMPTYH + EMPTYI + EMPTYJ + EMPTYK + EMPTYL;
			if (EMPTY >= ticket) {
				switch (cond2) {
				case "同排":
					if (row.equals("H")) {
						if (EMPTYH >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("I")) {
						if (EMPTYI >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("J")) {
						if (EMPTYJ >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("K")) {
						if (EMPTYK >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("L")) {
						if (EMPTYL >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else {
						System.out.println("row " + row + " 沒有最佳位置");
						result = false;
					}
					break;
				case "不需同排":
					result = true;
					break;
				default:
					result = false;
					System.out.println("輸入條件錯誤。");
					break;
				}
			} else {
				System.out.println("最佳位置區域已經沒有足後的座位了。");
				result = false;
			}
			break;
		case "精華區":
			// I row
			for (int i = 13; i < 25; i++) {
				if (I[i] == "O    ") {
					EMPTYI = EMPTYI + 1;
				}
			}
			// J row
			for (int i = 13; i < 25; i++) {
				if (J[i] == "O    ") {
					EMPTYJ = EMPTYJ + 1;
				}
			}
			EMPTY = EMPTYI + EMPTYJ;
			if (EMPTY >= ticket) {
				switch (cond2) {
				case "同排":
					if (row.equals("I")) {
						if (EMPTYI >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else if (row.equals("J")) {
						if (EMPTYJ >= ticket) {
							result = true;
						} else {
							result = false;
						}
					} else {
						System.out.println("row " + row + "沒有最佳位置。");
						result = false;
					}
					break;
				case "不需同排":
					result = true;
					break;
				default:
					result = false;
					System.out.println("輸入條件錯誤。");
					break;
				}

			} else {
				System.out.println("精華區已經沒有足後的座位了。");
				result = false;
			}
			break;
		default:
			result = false;
			System.out.println("輸入區域條件錯誤。");
		}
		return result;
	}

	/**
	 * The method is fucking invalid
	 * 
	 * @param ticket
	 * @param cond1
	 * @param cond2
	 * @param row
	 * @return
	 */
	public String normalbookseat(int ticket, String cond1, String cond2, String row) {
		String result = new String();
		if (cond1.equals("無特殊條件")) {
			int leasttotal = 0;
			// 指定排、條件
			if (ticket > 39 && ticket <= total) {
				row = "超過一排";
			}
			switch (row) {
			case "無特殊條件":
				int c = 1;
				if (ticket <= total) {
					while (c <= 13) {
						if (leasttotal >= ticket) {
							break;
						}
						if (c == 1) {
							leasttotal = totalA;
						} else if (c == 2) {
							leasttotal = totalA + totalB;
						} else if (c == 3) {
							leasttotal = totalA + totalB + totalC;
						} else if (c == 4) {
							leasttotal = totalA + totalB + totalC + totalD;
						} else if (c == 5) {
							leasttotal = totalA + totalB + totalC + totalD + totalE;
						} else if (c == 6) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF;
						} else if (c == 7) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG;
						} else if (c == 8) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH;
						} else if (c == 9) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI;
						} else if (c == 10) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ;
						} else if (c == 11) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK;
						} else if (c == 12) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK + totalL;
						} else if (c == 13) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK + totalL + totalM;
						}
						c = c + 1;
					}
				} else {
					result = "error, no enough seats";
					System.out.println("error, no enough seats");
				}
				if (c == 1) {
					int i = 0, k = 1;
					while (true) {
						if (k <= ticket) {
							if (A[i].equals("O    ")) {
								A[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 2) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalA) {
							if (B[i].equals("O    ")) {
								B[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 3) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA) {
							if (C[i].equals("O    ")) {
								C[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 4) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC) {
							if (D[i].equals("O    ")) {
								D[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 5) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD) {
							if (E[i].equals("O    ")) {
								E[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 6) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE) {
							if (F[i].equals("O    ")) {
								F[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 7) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF) {
							if (G[i].equals("O    ")) {
								G[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 8) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG) {
							if (H[i].equals("O    ")) {
								H[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 9) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH) {
							if (I[i].equals("O    ")) {
								I[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 10) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH
								- totalI) {
							if (J[i].equals("O    ")) {
								J[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 11) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ) {
							if (K[i].equals("O    ")) {
								K[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 12) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					for (int j = 0; j < K.length; j++) {
						if (K[j].equals("O    ")) {
							K[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ - totalK) {
							if (L[i].equals("O    ")) {
								L[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (c == 13) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					for (int j = 0; j < K.length; j++) {
						if (K[j].equals("O    ")) {
							K[j] = "X    ";
						}
					}
					for (int j = 0; j < L.length; j++) {
						if (L[j].equals("O    ")) {
							L[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ - totalK - totalL) {
							if (M[i].equals("O    ")) {
								M[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				}
			case "超過一排":
				int b = 1;
				if (ticket <= total) {
					while (b <= 13) {
						if (leasttotal >= ticket) {
							break;
						}
						if (b == 1) {
							leasttotal = totalA;
						} else if (b == 2) {
							leasttotal = totalA + totalB;
						} else if (b == 3) {
							leasttotal = totalA + totalB + totalC;
						} else if (b == 4) {
							leasttotal = totalA + totalB + totalC + totalD;
						} else if (b == 5) {
							leasttotal = totalA + totalB + totalC + totalD + totalE;
						} else if (b == 6) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF;
						} else if (b == 7) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG;
						} else if (b == 8) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH;
						} else if (b == 9) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI;
						} else if (b == 10) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ;
						} else if (b == 11) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK;
						} else if (b == 12) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK + totalL;
						} else if (b == 13) {
							leasttotal = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI
									+ totalJ + totalK + totalL + totalM;
						}
						b = b + 1;
					}
				} else {
					result = "error, no enough seats";
					System.out.println("error, no enough seats");
				}
				if (b == 1) {
					int i = 0, k = 1;
					while (true) {
						if (k <= ticket) {
							if (A[i].equals("O    ")) {
								A[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 2) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalA) {
							if (B[i].equals("O    ")) {
								B[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 3) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA) {
							if (C[i].equals("O    ")) {
								C[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 4) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC) {
							if (D[i].equals("O    ")) {
								D[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 5) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD) {
							if (E[i].equals("O    ")) {
								E[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 6) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE) {
							if (F[i].equals("O    ")) {
								F[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 7) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF) {
							if (G[i].equals("O    ")) {
								G[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 8) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG) {
							if (H[i].equals("O    ")) {
								H[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 9) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH) {
							if (I[i].equals("O    ")) {
								I[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 10) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH
								- totalI) {
							if (J[i].equals("O    ")) {
								J[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 11) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ) {
							if (K[i].equals("O    ")) {
								K[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 12) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					for (int j = 0; j < K.length; j++) {
						if (K[j].equals("O    ")) {
							K[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ - totalK) {
							if (L[i].equals("O    ")) {
								L[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				} else if (b == 13) {
					int i = 0, k = 1;
					for (int j = 0; j < A.length; j++) {
						if (A[j].equals("O    ")) {
							A[j] = "X    ";
						}
					}
					for (int j = 0; j < B.length; j++) {
						if (B[j].equals("O    ")) {
							B[j] = "X    ";
						}
					}
					for (int j = 0; j < C.length; j++) {
						if (C[j].equals("O    ")) {
							C[j] = "X    ";
						}
					}
					for (int j = 0; j < D.length; j++) {
						if (D[j].equals("O    ")) {
							D[j] = "X    ";
						}
					}
					for (int j = 0; j < E.length; j++) {
						if (E[j].equals("O    ")) {
							E[j] = "X    ";
						}
					}
					for (int j = 0; j < F.length; j++) {
						if (F[j].equals("O    ")) {
							F[j] = "X    ";
						}
					}
					for (int j = 0; j < G.length; j++) {
						if (G[j].equals("O    ")) {
							G[j] = "X    ";
						}
					}
					for (int j = 0; j < H.length; j++) {
						if (H[j].equals("O    ")) {
							H[j] = "X    ";
						}
					}
					for (int j = 0; j < I.length; j++) {
						if (I[j].equals("O    ")) {
							I[j] = "X    ";
						}
					}
					for (int j = 0; j < J.length; j++) {
						if (J[j].equals("O    ")) {
							J[j] = "X    ";
						}
					}
					for (int j = 0; j < K.length; j++) {
						if (K[j].equals("O    ")) {
							K[j] = "X    ";
						}
					}
					for (int j = 0; j < L.length; j++) {
						if (L[j].equals("O    ")) {
							L[j] = "X    ";
						}
					}
					while (true) {
						if (k <= ticket - totalB - totalA - totalC - totalD - totalE - totalF - totalG - totalH - totalI
								- totalJ - totalK - totalL) {
							if (M[i].equals("O    ")) {
								M[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "book successfully";
					System.out.println("book successfully");
				}
			case "A":
				if (ticket >= A.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalA) {
						result = "error, the number of seats on row A isn't enough";
					} else if (ticket <= totalA) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (A[i].equals("O    ")) {
									A[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "B":
				if (ticket >= B.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalB) {
						result = "error, the number of seats on row B isn't enough";
					} else if (ticket <= totalB) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (B[i].equals("O    ")) {
									B[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "C":
				if (ticket >= C.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalC) {
						result = "error, the number of seats on row C isn't enough";
					} else if (ticket <= totalC) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (C[i].equals("O    ")) {
									C[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "D":
				if (ticket >= D.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalD) {
						result = "error, the number of seats on row D isn't enough";
					} else if (ticket <= totalD) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (D[i].equals("O    ")) {
									D[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "E":
				if (ticket >= E.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalE) {
						result = "error, the number of seats on row E isn't enough";
					} else if (ticket <= totalE) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (E[i].equals("O    ")) {
									E[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "F":
				if (ticket >= F.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalF) {
						result = "error, the number of seats on row F isn't enough";
					} else if (ticket <= totalF) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (F[i].equals("O    ")) {
									F[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "G":
				if (ticket >= G.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalG) {
						result = "error, the number of seats on row G isn't enough";
					} else if (ticket <= totalG) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (G[i].equals("O    ")) {
									G[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "H":
				if (ticket >= H.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalH) {
						result = "error, the number of seats on row H isn't enough";
					} else if (ticket <= totalH) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (H[i].equals("O    ")) {
									H[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "I":
				if (ticket >= I.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalI) {
						result = "error, the number of seats on row I isn't enough";
					} else if (ticket <= totalI) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (I[i].equals("O    ")) {
									I[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "J":
				if (ticket >= J.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalJ) {
						result = "error, the number of seats on row J isn't enough";
					} else if (ticket <= totalJ) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (J[i].equals("O    ")) {
									J[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "K":
				if (ticket >= K.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalK) {
						result = "error, the number of seats on row K isn't enough";
					} else if (ticket <= totalK) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (K[i].equals("O    ")) {
									K[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "L":
				if (ticket >= L.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalL) {
						result = "error, the number of seats on row L isn't enough";
					} else if (ticket <= totalL) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (L[i].equals("O    ")) {
									L[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			case "M":
				if (ticket >= M.length) {
					// 如果票數超過座位數，回傳ERROR
					result = "error, the number of the seat isn't exist";
				} else {
					// 票數小於座位數
					if (ticket > totalM) {
						result = "error, the number of seats on row M isn't enough";
					} else if (ticket <= totalM) {
						int i = 0, k = 1;
						while (true) {
							if (k <= ticket) {
								if (M[i].equals("O    ")) {
									M[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "book successfully";
						System.out.println("book successfully");
					}
				}
				break;
			default:
				// 如果排數輸入錯誤，輸出Error
				result = "error, there's no " + row + " row.";
				break;

			}
		} else if (cond1.equals("精華區")) {
			int countI = 0, countJ = 0;
			switch (cond2) {
			case "同排":
				switch (row) {
				case "I":
					for (int q = 0; q < 8; q++) {
						if (I[q].equals("O    ")) {
							countI += 1;
						}
					}
					if (countI >= ticket) {
						int i = 13, k = 1;
						while (true) {
							if (k <= ticket) {
								if (I[i].equals("O   ")) {
									I[i] = "X    ";
									i = i + 1;
									k = k + 1;
								}
							} else {
								break;
							}
						}
						result = "訂票成功";
					} else {
						System.out.println("I排的精華區已無位置");
						result = "I排的精華區已無位置";
					}
					break;
				case "J":
					for (int q = 0; q < 8; q++) {
						if (J[q].equals("O    ")) {
							countJ += 1;
						}
					}
					if (countJ >= ticket) {
						int j = 13, l = 1;
						while (true) {
							if (l <= ticket) {
								if (J[j].equals("O   ")) {
									J[j] = "X    ";
									j = j + 1;
									l = l + 1;
								}
							} else {
								break;
							}
						}
						result = "訂票成功";
					} else {
						System.out.println("J排的精華區已無位置");
						result = "JJ排的精華區已無位置";
					}
					break;
				default:
					result = "輸入排數錯誤";
					break;
				}
			case "無特殊條件":
				for (int q = 0; q < 8; q++) {
					if (I[q].equals("O    ")) {
						countI += 1;
					}
				}
				if (countI >= ticket) {
					int i = 13, k = 1;
					while (true) {
						if (k <= ticket) {
							if (I[i].equals("O   ")) {
								I[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "訂票成功";
				} else {
					int i = 13, k = 1;
					for (int u = 13; u < 25; u++) {
						I[u] = "X    ";
					}
					while (true) {
						if (k <= ticket - countI) {
							if (J[i].equals("O   ")) {
								J[i] = "X    ";
								i = i + 1;
								k = k + 1;
							}
						} else {
							break;
						}
					}
					result = "訂票成功";
				}
				break;
			}
		} else if (cond1.equals("最佳位置")) {
			// 不選排還沒寫
			switch (row) {
			case "H":
				int i = 9, k = 1;
				while (true) {
					if (k <= ticket) {
						if (H[i].equals("O   ")) {
							H[i] = "X    ";
							i = i + 1;
							k = k + 1;
						}
					} else {
						break;
					}
				}
				break;
			case "I":
				int a = 9, b = 1;
				for (a = 9; a < 11; a++) {
					if (b <= ticket) {
						if (I[a].equals("O    ")) {
							I[a] = "X    ";
							b = b + 1;
						}
					} else {
						break;
					}

				}
				for (int c = 27; c < 29; c++) {
					if (b <= ticket) {
						if (I[c].equals("O    ")) {
							I[c] = "X    ";
							b = b + 1;
						}
					} else {
						break;
					}
				}
				break;
			case "J":
				int d = 9, e = 1;
				for (d = 9; d < 11; d++) {
					if (e <= ticket) {
						if (I[d].equals("O    ")) {
							I[d] = "X    ";
							e = e + 1;
						}
					} else {
						break;
					}
				}
				for (int c = 27; c < 29; c++) {
					if (e <= ticket) {
						if (I[c].equals("O    ")) {
							I[c] = "X    ";
							e = e + 1;
						}
					} else {
						break;
					}
				}
				break;
			case "K":
				int f = 9, g = 1;
				while (true) {
					if (g <= ticket) {
						if (H[f].equals("O   ")) {
							H[f] = "X    ";
							f = f + 1;
							g = g + 1;
						}
					} else {
						break;
					}
				}
				break;
			case "L":
				int h = 9, m = 1;
				while (true) {
					if (m <= ticket) {
						if (L[h].equals("O    ")) {
							L[h] = "X    ";
							h = h + 1;
							m = m + 1;
						}
					} else {
						break;
					}
				}
				break;
			}
		} else if (cond1.equals("次佳位置")) {
			for (int i = 7; i < 31; i++) {
				if (G[i].equals("O    ")) {
					G[i] = "X    ";
					ticket = ticket - 1;
				}
			}
			if (ticket > 0) {
				for (int i = 7; i < 9; i++) {
					if (H[i].equals("O    ")) {
						H[i] = "X    ";
						ticket = ticket - 1;
					}
				}
				if (ticket > 0) {
					for (int i = 29; i < 31; i++) {
						if (H[i].equals("O    ")) {
							H[i] = "X    ";
							ticket = ticket - 1;
						}
					}
					if (ticket > 0) {
						for (int i = 7; i < 9; i++) {
							if (I[i].equals("O    ")) {
								I[i] = "X    ";
								ticket = ticket - 1;
							}
						}
						if (ticket > 0) {
							for (int i = 29; i < 31; i++) {
								if (I[i].equals("O    ")) {
									I[i] = "X    ";
									ticket = ticket - 1;
								}
							}
							if (ticket > 0) {
								for (int i = 7; i < 9; i++) {
									if (J[i].equals("O    ")) {
										J[i] = "X    ";
										ticket = ticket - 1;
									}
								}
								if (ticket > 0) {
									for (int i = 29; i < 31; i++) {
										if (J[i].equals("O    ")) {
											J[i] = "X    ";
											ticket = ticket - 1;
										}
									}
									if (ticket > 0) {
										for (int i = 7; i < 9; i++) {
											if (K[i].equals("O    ")) {
												K[i] = "X    ";
												ticket = ticket - 1;
											}
										}
										if (ticket > 0) {
											for (int i = 29; i < 31; i++) {
												if (K[i].equals("O    ")) {
													K[i] = "X    ";
													ticket = ticket - 1;
												}
											}
											if (ticket > 0) {
												for (int i = 5; i < 9; i++) {
													if (L[i].equals("O    ")) {
														L[i] = "X    ";
														ticket = ticket - 1;
													}
												}
												if (ticket > 0) {
													for (int i = 31; i < 35; i++) {
														if (L[i].equals("O    ")) {
															L[i] = "X    ";
															ticket = ticket - 1;
														}
													}
												} else {
													result = "訂票成功";
												}
											} else {
												result = "訂票成功";
											}
										} else {
											result = "訂票成功";
										}
									} else {
										result = "訂票成功";
									}
								} else {
									result = "訂票成功";
								}
							} else {
								result = "訂票成功";
							}
						} else {
							result = "訂票成功";
						}
					} else {
						result = "訂票成功";
					}
				} else {
					result = "訂票成功";
				}
			} else {
				result = "訂票成功";
			}
		}

		return result;
	}

	/**
	 * The method provides normal booking
	 * @param ticket the number of tickets
	 * @return true or false whether the booking is successful or not
	 */
	public boolean bignormalbook(int ticket) {
		SEAT = "";
		boolean result = false;
		this.bigseatnum();
		if (ticket > total) {
			result = false;
			System.out.println("訂票失敗，座位已滿");
			return result;
		} else {
			num = ticket;
			int j = 0;
			row = new String[num];
			column2 = new int[num];
			for (int i = 0; i < 38; i++) {
				if (A[i].equals("O    ")) {
					A[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "A";
					column2[j] = i;
					SEAT = SEAT + "<A_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (B[i].equals("O    ")) {
					B[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "B";
					column2[j] = i;
					SEAT = SEAT + "<B_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (C[i].equals("O    ")) {
					C[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "C";
					column2[j] = i;
					SEAT = SEAT + "<C_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (D[i].equals("O    ")) {
					D[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "D";
					column2[j] = i;
					SEAT = SEAT + "<D_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (E[i].equals("O    ")) {
					E[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "E";
					column2[j] = i;
					SEAT = SEAT + "E_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (F[i].equals("O    ")) {
					F[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "F";
					column2[j] = i;
					j++;
					SEAT = SEAT + "<F_" + (i + 1) + "> ";
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (G[i].equals("O    ")) {
					G[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "G";
					column2[j] = i;
					SEAT = SEAT + "<G_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (H[i].equals("O    ")) {
					H[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "H";
					column2[j] = i;
					SEAT = SEAT + "<H_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (I[i].equals("O    ")) {
					I[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "I";
					column2[j] = i;
					SEAT = SEAT + "<I_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (J[i].equals("O    ")) {
					J[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "J";
					column2[j] = i;
					SEAT = SEAT + "<J_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (K[i].equals("O    ")) {
					K[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "K";
					column2[j] = i;
					SEAT = SEAT + "<K_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 39; i++) {
				if (L[i].equals("O    ")) {
					L[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "L";
					column2[j] = i;
					SEAT = SEAT + "<L_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int i = 0; i < 38; i++) {
				if (M[i].equals("O    ")) {
					M[i] = "X    ";
					ticket = ticket - 1;
					row[j] = "M";
					column2[j] = i;
					SEAT = SEAT + "<M_" + (i + 1) + "> ";
					j++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			j=0;
		}
		return result;
	}
	/**
	 * This method provides the unbook method of big movie room
	 * @param a the index of ticket
	 */
	public void bigunbook(MOVIETICKET a) {
		for (int i = 0; i < a.num; i++) {
			switch (a.row[i]) {
			case "A":
				A[a.column[i]] = "O    ";
				break;
			case "B":
				B[a.column[i]] = "O    ";
				break;
			case "C":
				C[a.column[i]] = "O    ";
				break;
			case "D":
				D[a.column[i]] = "O    ";
				break;
			case "E":
				E[a.column[i]] = "O    ";
				break;
			case "F":
				F[a.column[i]] = "O    ";
				break;
			case "G":
				G[a.column[i]] = "O    ";
				break;
			case "H":
				H[a.column[i]] = "O    ";
				break;
			case "I":
				I[a.column[i]] = "O    ";
				break;
			case "J":
				J[a.column[i]] = "O    ";
				break;
			case "K":
				K[a.column[i]] = "O    ";
				break;
			case "L":
				L[a.column[i]] = "O    ";
				break;
			case "M":
				M[a.column[i]] = "O    ";
				break;
			}
		}
	}
}