/**
 * The User class provides 20 people to use the booking system
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
public class User {
	public int index;
	public String name;
	public int age;
	/**
	 * This method provides 20 people to use the booking system
	 * @param index the index of users
	 */
	public User(int index){
		//constructor
		switch(index){
		case 0:
			this.index = index;
			this.name = "Wiggins Williamson";
			this.age = 62;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 1:
			this.index = index;
			this.name = "Robbie Tanner";
			this.age = 31;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 2:
			this.index = index;
			this.name = "Madden Levy";
			this.age = 9;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 3:
			this.index = index;
			this.name = "Hooper Roberts";
			this.age = 48;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 4:
			this.index = index;
			this.name = "Luella Oneil";
			this.age = 30;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 5:
			this.index = index;
			this.name = "Luisa Juarez";
			this.age = 17;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 6:
			this.index = index;
			this.name = "Lilly Navarro";
			this.age = 15;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 7:
			this.index = index;
			this.name = "Katina Horne";
			this.age = 38;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 8:
			this.index = index;
			this.name = "Katina Horne";
			this.age = 38;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 9:
			this.index = index;
			this.name = "Nellie Morin";
			this.age = 36;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 10:
			this.index = index;
			this.name = "Cote Mccray";
			this.age = 45;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 11:
			this.index = index;
			this.name = "Jackie Bradshaw";
			this.age = 52;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 12:
			this.index = index;
			this.name = "Opal Buckner";
			this.age = 16;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 13:
			this.index = index;
			this.name = "Catalina Franklin";
			this.age = 47;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 14:
			this.index = index;
			this.name = "Lillie Dennis";
			this.age = 13;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 15:
			this.index = index;
			this.name = "Louise Thomas";
			this.age = 67;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 16:
			this.index = index;
			this.name = "Blackburn Cantu";
			this.age = 32;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 17:
			this.index = index;
			this.name = "Dudley Harvey";
			this.age = 34;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 18:
			this.index = index;
			this.name = "Guzman Lee";
			this.age = 21;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		case 19:
			this.index = index;
			this.name = "Kayla Mccullough";
			this.age = 44;
			System.out.println("歡迎"+this.name+"使用本訂票系統。");
			break;
		default:
			System.out.println("失敗，這個人的ID不存在。");
			break;
			
		}
	}

}
