/**
 * The class provides the small movie room's information and some methods of it.
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Arrays;
public class smallroom {
	public String name;
	public String id;
	public String time;
	public int smallseatnum;
	public int playtime;
	public int[] column;
	public String[] A;
	public String[] B;
	public String[] C;
	public String[] D;
	public String[] E;
	public String[] F;
	public String[] G;
	public String[] H;
	public String[] I;
	public String SEAT = new String();
	int total = 0, totalA = 0, totalB = 0, totalC = 0, totalD = 0;
	int totalE = 0, totalF = 0, totalG = 0, totalH = 0, totalI = 0;
	public String[] row;
	public int[] column2;
	public int num = 0;
	/**
	 * The default constructor
	 */
	public smallroom() {
		this.name = new String("");
		this.time = new String("");
		this.id = new String("");
		this.playtime = 0;
		this.smallseatnum = 0;
		column = new int[16];
		A = new String[16];
		B = new String[16];
		C = new String[16];
		D = new String[16];
		E = new String[16];
		F = new String[16];
		G = new String[16];
		H = new String[16];
		I = new String[16];
	}
	/**
	 * The constructor
	 * @param name The movie room name
	 * @param time The movie length
	 */
	public smallroom(String name, String time) {
		// constructor
		// smallroom room = new smallroom();
		switch (name) {
		case "峨嵋":
			this.name = name;
			this.time = time;
			if (time.equals("17：50") || time.equals("21：30")) {
				this.playtime = 83;
				this.id = "WGd6f01Om27eSmo9X3b6cuXu";
			} else {
				this.playtime = 91;
				this.id = "pYXEwQEWFBBFarOD5LBJmnTU";
			}
			this.smallseatnum = 0;
			column = new int[16];
			A = new String[16];
			B = new String[16];
			C = new String[16];
			D = new String[16];
			E = new String[16];
			F = new String[16];
			G = new String[16];
			H = new String[16];
			I = new String[16];
			setsmallSEAT();
			break;
		case "崆峒":
			this.name = name;
			this.id = "x7GCv22RgYGfd5l8YdbVqYhZ";
			this.time = time;
			this.playtime = 161;
			this.smallseatnum = 0;
			column = new int[16];
			A = new String[16];
			B = new String[16];
			C = new String[16];
			D = new String[16];
			E = new String[16];
			F = new String[16];
			G = new String[16];
			H = new String[16];
			I = new String[16];
			setsmallSEAT();
			break;
		default:
			System.out.println("沒有這個影廳。");
			break;
		}
	}
	/**
	 * The method can check if the room has enough seats.
	 * @param seat The number of seats
	 * @param maxtime The maximum time
	 * @param mintime The minimum time
	 * @return true or false
	 */
	public boolean conditionOK(int seat, int maxtime, int mintime) {
		return this.smallseatnum() >= seat && this.playtime <= maxtime && this.playtime >= mintime;
	}
	/**
	 * The method creates the seating chart of the room
	 */
	public void setsmallSEAT() {
		// 建立座位表
		// A排到I排
		// 1號到16號
		for (int i = 0; i < column.length; i++) {
			column[i] = i + 1;
		}
		System.out.println(" ");
		for (int i = 0; i < A.length; i++) {
			A[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < B.length; i++) {
			B[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < C.length; i++) {
			C[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < D.length; i++) {
			D[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < E.length; i++) {
			E[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < F.length; i++) {
			F[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < G.length; i++) {
			G[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < H.length; i++) {
			H[i] = "O";
		}
		System.out.println(" ");
		for (int i = 0; i < I.length; i++) {
			I[i] = "O";
		}
		System.out.println(" ");

	}
	/**
	 * The method calculates the remain numbers of seats of the small movie room.
	 * @return the remain numbers of seats of the small movie room.
	 */
	public int smallseatnum() {
		// 剩餘座位數

		String judge = new String("O");
		for (int i = 0; i < A.length; i++) {
			if (A[i].equals(judge)) {
				totalA = totalA + 1;
			}
			// System.out.println("total = " + totalA);
			// 把A排剩餘空格數加起來得totalA
		}
		for (int i = 0; i < B.length; i++) {
			if (B[i].equals(judge)) {
				totalB = totalB + 1;
			}
		}
		for (int i = 0; i < C.length; i++) {
			if (C[i].equals(judge)) {
				totalC = totalC + 1;
			}
		}
		for (int i = 0; i < I.length; i++) {
			if (I[i].equals(judge)) {
				totalI = totalI + 1;
			}
		}
		for (int i = 0; i < D.length; i++) {
			if (D[i].equals(judge)) {
				totalD = totalD + 1;
			}
		}
		for (int i = 0; i < E.length; i++) {
			if (E[i].equals(judge)) {
				totalE = totalE + 1;
			}
		}
		for (int i = 0; i < F.length; i++) {
			if (F[i].equals(judge)) {
				totalF = totalF + 1;
			}
		}
		for (int i = 0; i < G.length; i++) {
			if (G[i].equals(judge)) {
				totalG = totalG + 1;
			}
		}
		for (int i = 0; i < H.length; i++) {
			if (H[i].equals(judge)) {
				totalH = totalH + 1;
			}
		}
		total = totalA + totalB + totalC + totalD + totalE + totalF + totalG + totalH + totalI;
		// System.out.println("現在總共有" + total + "個空位。");
		return total;
	}
	/**
	 * The method provides the condition when booking one seat
	 * @param row The assign row
	 * @param index The assign index
	 * @return  true or false if the booking is successful or not
	 */
	public boolean booksingleseat(String row, int index) {
		// 訂一個位置
		// 指定排、座號
		boolean result;
		if (index >= 16) {
			result = false;
			System.out.println("error, the number of the seat is out of bound.");
		}
		// 如果輸入座號超過16，則輸出Error
		else {
			// 排
			switch (row) {
			case "A":
				if (A[index].equals("O")) {
					// 如果該位置為空("O")，則改成有人("X")，並回傳訂位成功
					A[index] = "X";
					System.out.println("seat A_" + (index + 1) + "is booked successfully.");
					result = true;
				} else {
					// 如果該位置為有人("X")，並回傳Error
					System.out.println("error, the seat you chose is chosen.");
					result = false;
				}
				break;
			case "B":
				if (B[index].equals("O")) {
					B[index] = "X";
					result = true;
					System.out.println("seat B_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "C":
				if (C[index].equals("O")) {
					C[index] = "X";
					result = true;
					System.out.println("seat C_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "D":
				if (D[index].equals("O")) {
					D[index] = "X";
					result = true;
					System.out.println("seat D_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "E":
				if (E[index].equals("O")) {
					E[index] = "X";
					result = true;
					System.out.println("seat E_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "F":
				if (F[index].equals("O")) {
					F[index] = "X";
					result = true;
					System.out.println("seat F_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "G":
				if (G[index].equals("O")) {
					G[index] = "X";
					result = true;
					System.out.println("seat G_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "H":
				if (H[index].equals("O")) {
					H[index] = "X";
					result = true;
					System.out.println("seat H_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			case "I":
				if (I[index].equals("O")) {
					I[index] = "X";
					result = true;
					System.out.println("seat I_" + (index + 1) + "is booked successfully.");
				} else {
					result = false;
					System.out.println("error, the seat you chose is chosen.");
				}
				break;
			default:
				// 如果排數輸入錯誤，輸出Error
				result = false;
				System.out.println("error, there's no " + row + " row.");
				break;

			}
		}
		return result;
	}
	/**
	 * The method provides normal booking
	 * @param ticket the number of tickets
	 * @return true or false whether the booking is successful or not
	 */
	public boolean smallnormalbook(int ticket) {
		boolean result = false;
		this.smallseatnum();
		if (ticket > total) {
			System.out.println("訂票失敗，座位已滿");
			result = false;
			return result;
		} else {
			num = ticket;
			int i=0;
			row = new String[num];
			column2 = new int[num];
			for (int a = 0; a < 16; a++) {
				if (A[a].equals("O")) {
					A[a] = "X";
					ticket = ticket - 1;
					row[i] = "A";
					column2[i] = a;
					System.out.println(column2[i]);
					SEAT = SEAT + "<A_" + (a + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int b = 0; b < 16; b++) {
				if (B[b].equals("O")) {
					B[b] = "X";
					ticket = ticket - 1;
					row[i] = "B";
					column2[i] = b;
					SEAT = SEAT + "<B_" + (b + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int c = 0; c < 16; c++) {
				if (C[c].equals("O")) {
					C[c] = "X";
					ticket = ticket - 1;
					row[i] = "C";
					column2[i] = c;
					SEAT = SEAT + "<C_" + (c + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int d = 0; d < 16; d++) {
				if (D[d].equals("O")) {
					D[d] = "X";
					ticket = ticket - 1;
					row[i] = "D";
					column2[i] = d;
					SEAT = SEAT + "<D_" + (d + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int e = 0; e < 16; e++) {
				if (E[e].equals("O")) {
					E[e] = "X";
					ticket = ticket - 1;
					row[i] = "E";
					column2[i] = e;
					SEAT = SEAT + "<E_" + (e + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int f = 0; f < 16; f++) {
				if (F[f].equals("O")) {
					F[f] = "X";
					ticket = ticket - 1;
					row[i] = "F";
					column2[i] = f;
					SEAT = SEAT + "<F_" + (f + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int g = 0; g < 16; g++) {
				if (G[g].equals("O")) {
					G[g] = "X";
					ticket = ticket - 1;
					row[i] = "G";
					column2[i] = g;
					SEAT = SEAT + "<G_" + (g + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int h = 0; h < 16; h++) {
				if (H[h].equals("O")) {
					H[h] = "X";
					ticket = ticket - 1;
					row[i] = "H";
					column2[i] = h;
					SEAT = SEAT + "<H_" + (h + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			for (int j = 0; j < 16; j++) {
				if (I[j].equals("O")) {
					I[j] = "X";
					ticket = ticket - 1;
					row[i] = "I";
					column2[i] = j;
					SEAT = SEAT + "<I_" + (i + 1) + "> ";
					i++;
					if (ticket == 0) {
						result = true;
						System.out.println("訂票成功");
						return result;
					} else {
						continue;
					}
				}
			}
			i=0;
		}
		return result;
	}
	/**
	 * The method can unbook the ticket we input
	 * @param a the ticket index
	 */
	public void smallunbook(MOVIETICKET a){
		for (int i = 0; i < a.num; i++){
			switch (a.row[i]){
			case "A":
				A[a.column[i]] = "O";
				//System.out.println(i+"done");
				//System.out.println(a.column[i]+"done");
				break;
			case "B":
				B[a.column[i]] = "O";
				break;
			case "C":
				C[a.column[i]] = "O";
				break;
			case "D":
				D[a.column[i]] = "O";
				break;
			case "E":
				E[a.column[i]] = "O";
				break;
			case "F":
				F[a.column[i]] = "O";
				break;
			case "G":
				G[a.column[i]] = "O";
				break;
			case "H":
				H[a.column[i]] = "O";
				break;
			case "I":
				I[a.column[i]] = "O";
				break;
			}
		}
	}
	/**
	 * The method provides conditional booking of small room
	 * @param ticket the number of tickets
	 * @param cond1 the first condition
	 * @param cond2 the second condition
	 * @return
	 */
	public boolean smallconditionbook(int ticket, String cond1, String cond2){
		//cond1 排數
		//cond2 連續
		row = new String[ticket];
		column2 = new int[ticket];
		num=ticket;
		SEAT="";
		boolean result = false;
		this.smallseatnum();
		if (ticket > total) {
			System.out.println("訂票失敗，座位已滿");
			result = false;
			return result;
		}
		else{
			int x=0;
			switch(cond1){
			case "A":
				
				if(ticket > totalA){
					System.out.println("A排已經沒有 " + ticket + " 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (A[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							A[i] = "X";
							row[x]="A";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<A_" + i+1 + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
				
				
			case "B":
				if(ticket > totalB){
					System.out.println("B排已經沒有 " +ticket + " 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (B[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							B[i] = "X";
							row[x]="B";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<B_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "C":
				if(ticket > totalC){
					System.out.println("C排已經沒有 " +ticket + " 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (C[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							C[i] = "X";
							row[x]="C";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<C_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "D":
				if(ticket > totalD){
					System.out.println("D排已經沒有 " +ticket + " 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (D[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							D[i] = "X";
							row[x]="D";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<D_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "E":
				if(ticket > totalE){
					System.out.println("E排已經沒有 " + ticket +" 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (E[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							E[i] = "X";
							row[x]="E";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<E_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "F":
				if(ticket > totalF){
					System.out.println("F排已經沒有 " + ticket +" 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (F[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							F[i] = "X";
							row[x]="F";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<F_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "G":
				if(ticket > totalG){
					System.out.println("G排已經沒有 " + ticket +" 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (G[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							G[i] = "X";
							row[x]="G";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<G_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "H":
				if(ticket > totalH){
					System.out.println("H排已經沒有 " + ticket +" 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (H[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							H[i] = "X";
							row[x]="H";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<H_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			case "I":
				if(ticket > totalI){
					System.out.println("I排已經沒有 " + ticket +" 個位置了");
					result = false;
					return result;
				}
				else {
					int a = 0, maxlen = 0, position = 0;
					for (int i = 0; i <= 15; i++) {
						if (I[i].equals("X"))
							a = 0;
						else if (i == 4 || i == 12)
							a = 1;
						else
							a++;
						if (a > maxlen) {
							maxlen = a;
							position = i;
						}

					}
					if (maxlen >= ticket) {
						for (int i = (position - maxlen + 1); i <= (position - maxlen + ticket); i++) {
							I[i] = "X";
							row[x]="I";
							column2[x]=i;
							x++;
							System.out.println("訂票成功" + (i+1));
							SEAT = SEAT + "<I_" + i + "> ";
							result = true;
						}
						

					}
					else{
						System.out.println("連續座位不足");
						result=false;
					}

					
					break;

				}
			default:
				System.out.println("輸入排數錯誤");
				result = false;
				return result;
			}
		}
		return result;
	}
}
