/**
 * The conditionfind class provides some method to fit the needed condition of various movie room
 * @author 唐國霖、蘇柏燁、黃博鈞
 * @since 2017-06-25
 * @version 1.0
 */
public class conditionfind {
	/**
	* The method can change the time of Sting type to Integer type.
	 * @param time the String that has "hh:mm"form.
	 * @return Integer type of time.
	 * @throws MovieFuckFuckException
	 */
	public static int timetoint(String time)throws MovieFuckFuckException{
		String a[] = time.split("：");
		if(a.length != 2){
			throw new MovieFuckFuckException("幹你老師全形冒號不會打喔");
			}
			else{
		int h = Integer.valueOf(a[0]);
		int m = Integer.valueOf(a[1]);
		return h*60+ m;
			}
		
	}
	/**
	 *  * The method can seek the movies which play time between the input two times and prints it.
	 * @param time1 the earlier time
	 * @param time2 the later time
	 */
	public void timecompare(String time1, String time2){
		int early;
		try {
			early = timetoint(time1);
			int late = timetoint(time2);
			String result;
			movieinfo A = new movieinfo("3R47wXhjjLqnLWef6HU155ek");
			movieinfo B = new movieinfo("H55WYTppJHIwZyCfUHJLIbWC");
			movieinfo C = new movieinfo("Q9xwOoSHCD0Xwfm2EiDGRn3Z");
			movieinfo D = new movieinfo("WGd6f01Om27eSmo9X3b6cuXu");
			movieinfo E = new movieinfo("pYXEwQEWFBBFarOD5LBJmnTU");
			movieinfo F = new movieinfo("x7GCv22RgYGfd5l8YdbVqYhZ");

			// A
			result = "";
			for (String i : A.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = A.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}

			// B
			result = "";
			for (String i : B.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = B.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}

			// C
			result = "";
			for (String i : C.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = C.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}

			// D
			result = "";
			for (String i : D.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = D.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}

			// E
			result = "";
			for (String i : E.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = E.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}

			// F
			result = "";
			for (String i : F.time2) {
				if (timetoint(i) >= early && timetoint(i) <= late)
					result = result + "<" + i + ">";
			}
			if (!result.equals("")) {
				result = F.id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}
		} catch (MovieFuckFuckException e) {
			// TODO Auto-generated catch block
			//System.out.println(e.getMessage());
		}
		
	}
	/**
	 * The method searchs the movie that fits condition and prints the movie id and the play time of big movie room
	 * @param B the big movie room
	 * @param num the ticket number
	 * @param time1 the earlier time 
	 * @param time2 the later time
	 * @param maxtime the maximum movie length
	 * @param mintime the minimum movie length
	 */
	public void bigseatcondition(bigroom[] B, int num, String time1, String time2, int maxtime, int mintime){
		String result = new String("");
		try {
			int early = timetoint(time1);
			int late = timetoint(time2);
			for (bigroom i : B) {
				if (i.conditionOK(num, maxtime, mintime) && timetoint(i.time) >= early && timetoint(i.time) <= late)
					result = result + "<" + i.time + ">";
			}
			if (!result.equals("")) {
				result = B[0].id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}
		} catch (MovieFuckFuckException e) {
			// TODO Auto-generated catch block
			//System.out.println(e.getMessage());
		}
		
		
		
	}
	/**
	 * The method searchs the movie that fits condition and prints the movie id and the play time of small movie room
	 * @param B the small movie room
	 * @param num the ticket number
	 * @param time1 the earlier time 
	 * @param time2 the later time
	 * @param maxtime the maximum movie length
	 * @param mintime the minimum movie length
	 */
	public void smallseatcondition(smallroom[] B, int num, String time1, String time2, int maxtime, int mintime){
		String result = new String("");
	
		try {
			int early = timetoint(time1);
			int late = timetoint(time2);
			for (smallroom i : B) {
				if (i.conditionOK(num, maxtime, mintime) && timetoint(i.time) >= early && timetoint(i.time) <= late)
					result = result + "<" + i.time + ">";
			}
			if (!result.equals("")) {
				result = B[0].id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}
		} catch (MovieFuckFuckException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
		
	}
	/**
	 * The method searchs the movie that fits condition and prints the movie id and the play time of "峨眉"movie room
	 * @param B the small movie room
	 * @param num the ticket number
	 * @param time1 the earlier time 
	 * @param time2 the later time
	 * @param maxtime the maximum movie length
	 * @param mintime the minimum movie length
	 */
	public void ermeicondition(smallroom[] B, int num, String time1, String time2, int maxtime, int mintime){
		String result = new String("");
		
		try {
			int early = timetoint(time1);
			int late = timetoint(time2);
			for (int i = 0; i <= 2; i++) {
				if (B[i].conditionOK(num, maxtime, mintime) && timetoint(B[i].time) >= early
						&& timetoint(B[i].time) <= late)
					result = result + "<" + B[i].time + ">";
			}
			if (!result.equals("")) {
				result = B[0].id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}
			result = "";
			for (int i = 3; i <= 4; i++) {
				if (B[i].conditionOK(num, maxtime, mintime) && timetoint(B[i].time) >= early
						&& timetoint(B[i].time) <= late)
					result = result + "<" + B[i].time + ">";
			}
			if (!result.equals("")) {
				result = B[3].id + "：" + result;
				System.out.print(result);
				System.out.println("");
			}
		} catch (MovieFuckFuckException e) {
			// TODO Auto-generated catch block
		}
		
		
	}
	
}