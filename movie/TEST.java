import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;

public class Test {

	public static void main(String[] args) {

		// bigroom[] B = {new bigroom("武當","09：40"), new bigroom("武當","12：00"),
		// new bigroom("武當","14：20"), new bigroom("武當","16：40"), new
		// bigroom("武當","19：00"), new bigroom("武當","21：20"), new
		// bigroom("武當","23：40")};
		// smallroom S = new smallroom("峨嵋");
		// System.out.println("廳名" + B[0].name);
		// System.out.println("第B排第5個 = " + B[0].B[4]);
		//
		// System.out.println(B[0].bigseatnum());
		//
		//// System.out.println(B.normalbookseat(5, "無特殊條件", "同排", "A"));
		//
		// System.out.println("廳名" + S.name);
		// System.out.println("第B排第5個 = " + S.B[4]);
		// System.out.println(S.booksingleseat("A", 15));
		// System.out.println(S.booksingleseat("A", 15));
		// System.out.println(S.booksingleseat("A", 16));
		// System.out.println(S.booksingleseat("p", 15));
		//
		//// System.out.println(B.normalbookseat("A", 15));
		//// System.out.println(B.normalbookseat("A", 15));
		//// System.out.println(B.normalbookseat("A", 40));
		//// System.out.println(B.normalbookseat("p", 15));
		//
		// movieinfo movie = new movieinfo("Q9xwOoSHCD0Xwfm2EiDGRn3Z");
		// movieinfo movie2 = new movieinfo("123456789");
		// System.out.println(movieinfo.movierank(9.2));
		// System.out.println(movieinfo.movierank(8.5));
		// System.out.println(movieinfo.movierank(5.7));
		// System.out.println(movieinfo.movierank(10.6));
		//
		// movieinfo.movieinfo("x7GCv22RgYGfd5l8YdbVqYhZ");
		// movieinfo.movieinfo("Q9xwOoSHCD0Xwfm2EiDGRn3Z");
		// movieinfo.movieinfo("H55WYTppJHIwZyCfUHJLIbWC");
		// movieinfo.movieinfo("2326sdf2");
		//
		// conditionfind a =new conditionfind();
		// a.timecompare("08：00","18：00");

		bigroom[] woodon = { new bigroom("武當", "09：40"), new bigroom("武當", "12：00"), new bigroom("武當", "14：20"),
				new bigroom("武當", "16：40"), new bigroom("武當", "19：00"), new bigroom("武當", "21：20"),
				new bigroom("武當", "23：40") };
		bigroom[] shoulin = { new bigroom("少林", "09：05"), new bigroom("少林", "11：30"), new bigroom("少林", "14：45"),
				new bigroom("少林", "17：10"), new bigroom("少林", "19：35"), new bigroom("少林", "22：05") };
		bigroom[] huashine = { new bigroom("華山", "11：20"), new bigroom("華山", "15：20"), new bigroom("華山", "19：10"),
				new bigroom("華山", "23：00") };
		smallroom[] ermei = { new smallroom("峨嵋", "09：10"), new smallroom("峨嵋", "11：15"), new smallroom("峨嵋", "13：00"),
				new smallroom("峨嵋", "17：50"), new smallroom("峨嵋", "21：30") };
		smallroom[] hongtong = { new smallroom("崆峒", "10：50"), new smallroom("崆峒", "13：50"),
				new smallroom("崆峒", "16：50"), new smallroom("崆峒", "19：50"), new smallroom("崆峒", "21：50") };
		Scanner scanner = new Scanner(System.in);
		ArrayList<MOVIETICKET> ticketable = new ArrayList<MOVIETICKET>();
		for (int i = 0; i < 500; i++){
			ticketable.add(null);
		}
		int manid;
		User A;

back1:	while (true) {
			int num1, num2;

			System.out.println("請選擇功能(以數字表示)：");
			System.out.println("1. 一般訂票");
			System.out.println("2. 條件訂票");
			System.out.println("3. 電影查詢");
			System.out.println("4. 電影票查詢");
			System.out.println("5. 退票");
			System.out.println("6. 結束");
			num1 = scanner.nextInt();
			switch (num1) {
			case 1:
				String movieid, movietime, hall;
				while (true) {
					System.out.println("請輸入你的ID：");
					manid = scanner.nextInt();
					if (!(manid >= 20 || manid < 0)) {
						A = new User(manid);
						break;
					}
				}
				movieinfo temp;
				while (true) {
					System.out.println("請輸入電影ID：");
					movieid = scanner.next();
					temp = new movieinfo(movieid);
					if (!temp.judge) {
						System.out.println("電影ID錯誤，請重新輸入");
						continue;
					}
					else {
						break;
					}
				}
				
				if (A.age < temp.classification){
					System.out.println("訂票失敗，"+ A.age + "歲無法訂購"+temp.classification2+"的票");
					break;
				}
				switch (movieid) {
				case "3R47wXhjjLqnLWef6HU155ek":
					System.out.println("放映時間：09：40、12：00、14：20、16：40、19：00、21：20、23：40");
					break;
				case "H55WYTppJHIwZyCfUHJLIbWC":
					System.out.println("放映時間：09：05、11：30、14：45、17：10、19：35、22：05");
					break;
				case "Q9xwOoSHCD0Xwfm2EiDGRn3Z":
					System.out.println("放映時間：11：20、15：20、19：10、23：00");
					break;
				case "WGd6f01Om27eSmo9X3b6cuXu":
					System.out.println("放映時間：09：10、11：15、13：00");
					break;
				case "pYXEwQEWFBBFarOD5LBJmnTU":
					System.out.println("放映時間：17：50、21：30");
					break;
				case "x7GCv22RgYGfd5l8YdbVqYhZ":
					System.out.println("放映時間：10：50、13：50、16：50、19：50、21：50");
					break;
				}
				back2: while (true) {
					System.out.println("請輸入場次時間(XX：XX)：");
					movietime = scanner.next();
					hall = temp.hall;
					switch (hall) {
					case "武當":
						for (int i = 0; i < woodon.length; i++) {
							if (woodon[i].time.equals(movietime)) {
								int num;
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								boolean fuck =woodon[i].bignormalbook(num);
								String seat = woodon[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									System.out.println("///"+A.index);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp.movie, movietime, hall,
											seat, woodon[i].row,  woodon[i].column2,  woodon[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back2;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					case "少林":
						for (int i = 0; i < shoulin.length; i++) {
							if (shoulin[i].time.equals(movietime)) {
								int num;
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								boolean fuck = shoulin[i].bignormalbook(num);
								String seat = shoulin[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp.movie, movietime, hall,
											seat, shoulin[i].row,  shoulin[i].column2,  shoulin[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back2;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					case "華山":
						for (int i = 0; i < huashine.length; i++) {
							if (huashine[i].time.equals(movietime)) {
								int num;
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								boolean fuck = huashine[i].bignormalbook(num);
								String seat = huashine[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp.movie, movietime, hall,
											seat, huashine[i].row,  huashine[i].column2,  huashine[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back2;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					case "峨嵋":
						for (int i = 0; i < ermei.length; i++) {
							if (ermei[i].time.equals(movietime)) {
								int num;
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								boolean fuck = ermei[i].smallnormalbook(num);
								String seat = ermei[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp.movie, movietime, hall,
											seat, ermei[i].row,  ermei[i].column2,  ermei[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back2;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					case "崆峒":
						for (int i = 0; i < hongtong.length; i++) {
							if (hongtong[i].time.equals(movietime)) {
								int num;
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								boolean fuck = hongtong[i].smallnormalbook(num);
								String seat = hongtong[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp.movie, movietime, hall,
											seat, hongtong[i].row,  hongtong[i].column2,  hongtong[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back2;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					}
					break;
				}
				break;
			case 2:
				String movieid2, movietime2, hall2;
				while (true) {
					System.out.println("請輸入你的ID：");
					manid = scanner.nextInt();
					if (!(manid >= 20 || manid < 0)) {
						A = new User(manid);
						break;
					}
				}
				movieinfo temp2;
				while (true) {
					System.out.println("請輸入電影ID：");
					movieid = scanner.next();
					temp2 = new movieinfo(movieid);
					if (!temp2.judge) {
						System.out.println("電影ID錯誤，請重新輸入");
						continue;
					}
					else {
						break;
					}
				}
				if (A.age < temp2.classification){
					System.out.println("訂票失敗，"+ A.age + "歲無法訂購"+temp2.classification2+"的票");
					break;
				}
				switch (movieid) {
				case "3R47wXhjjLqnLWef6HU155ek":
					System.out.println("放映時間：09：40、12：00、14：20、16：40、19：00、21：20、23：40");
					break;
				case "H55WYTppJHIwZyCfUHJLIbWC":
					System.out.println("放映時間：09：05、11：30、14：45、17：10、19：35、22：05");
					break;
				case "Q9xwOoSHCD0Xwfm2EiDGRn3Z":
					System.out.println("放映時間：11：20、15：20、19：10、23：00");
					break;
				case "WGd6f01Om27eSmo9X3b6cuXu":
					System.out.println("放映時間：09：10、11：15、13：00");
					break;
				case "pYXEwQEWFBBFarOD5LBJmnTU":
					System.out.println("放映時間：17：50、21：30");
					break;
				case "x7GCv22RgYGfd5l8YdbVqYhZ":
					System.out.println("放映時間：10：50、13：50、16：50、19：50、21：50");
					break;
				}
		back3:	while (true) {
					System.out.println("請輸入場次時間(XX：XX)：");
					movietime = scanner.next();
					hall = temp2.hall;
					switch (hall) {
					case "武當":
						System.out.println("404 not found");						
						break;
					case "少林":
						System.out.println("404 not found");	
						break;
					case "華山":
						System.out.println("404 not found");	
						break;
					case "峨嵋":
						for (int i = 0; i < ermei.length; i++) {
							if (ermei[i].time.equals(movietime)) {
								int num;
								String cond1, cond2 = "連續";
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								System.out.println("請輸入排數(大寫A~I)：");
								cond1 = scanner.next();
								boolean fuck=ermei[i].smallconditionbook(num, cond1, cond2);
								String seat = ermei[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp2.movie, movietime, hall,
											seat, ermei[i].row,  ermei[i].column2,  ermei[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back3;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					case "崆峒":
						for (int i = 0; i < hongtong.length; i++) {
							if (hongtong[i].time.equals(movietime)) {
								int num;
								String cond1, cond2 = "連續";
								//String row;
								// System.out.println("請輸入列數：");
								// row = scanner.next();
								System.out.println("請輸入欲訂票數：");
								num = scanner.nextInt();
								System.out.println("請輸入排數(大寫A~I)：");
								cond1 = scanner.next();
								boolean fuck=hongtong[i].smallconditionbook(num, cond1, cond2);
								String seat = hongtong[i].SEAT;
								if (fuck) {
									int index = ticketable.indexOf(null);
									MOVIETICKET movie1 = new MOVIETICKET(A.index, index, temp2.movie, movietime, hall,
											seat, hongtong[i].row,  hongtong[i].column2,  hongtong[i].num);
									System.out.println("你的電影票ID：" + index);
									ticketable.set(index, movie1);
									break;
								} else {
									int work;
									System.out.println("請選擇執行事項(以數字表示)：");
									System.out.println("1. 重新選擇時間");
									System.out.println("2. 取消訂票");
									work = scanner.nextInt();
									switch (work) {
									case 1:
										continue back3;
									case 2:
										break;
									}
								}
								break;
							}
						}
						break;
					}
					break;
				}
				break;
			case 3:
				back: {
					while (true) {
						System.out.println("請選擇查詢事項(以數字表示)：");
						System.out.println("1. 電影資訊");
						System.out.println("2. 電影評分");
						System.out.println("3. 場次查詢");
						System.out.println("4. 返回");
						num2 = scanner.nextInt();
						switch (num2) {
						case 1:
							String a;
							System.out.println("請輸入電影ID：");
							a = scanner.next();
							movieinfo.movieinfo(a);
							break;
						case 2:
							double rank;
							System.out.println("請輸入評分：");
							rank = scanner.nextDouble();
							movieinfo.movierank(rank);
							break;
						case 3:

							conditionfind condition = new conditionfind();
							int ticket, maxtime, mintime;
							String earlytime, latetime;
							System.out.println("請輸入需求座位數：");
							ticket = scanner.nextInt();
							System.out.println("請輸入最早播映時間(XX：YY)，若無需求請輸入00：00");
							earlytime = scanner.next();
							System.out.println("請輸入最晚播映時間(XX：YY)，若無需求請輸入23：59");
							latetime = scanner.next();
							System.out.println("請輸入電影片長上限(分鐘)，若無需求請輸入666");
							maxtime = scanner.nextInt();
							System.out.println("請輸入電影片長下限(分鐘)，若無需求請輸入0");
							mintime = scanner.nextInt();
							condition.bigseatcondition(woodon, ticket, earlytime, latetime, maxtime, mintime);
							condition.bigseatcondition(shoulin, ticket, earlytime, latetime, maxtime, mintime);
							condition.bigseatcondition(huashine, ticket, earlytime, latetime, maxtime, mintime);
							condition.ermeicondition(ermei, ticket, earlytime, latetime, maxtime, mintime);
							condition.smallseatcondition(hongtong, ticket, earlytime, latetime, maxtime, mintime);

							break;
						case 4:
							break back;
						default:
							System.out.println("輸入錯誤，請重新輸入。");
							break;
						}
					}

				}

				break;
			case 4:
				int userid;
				int ticketid;
				MOVIETICKET ticket;
				while (true) {
					System.out.println("請輸入你的ID：");
					userid = scanner.nextInt();
					System.out.println("請輸入電影票ID：");
					ticketid = scanner.nextInt();
					if (ticketable.get(ticketid) == null) {
						int num;
						System.out.println("輸入電影票ID錯誤，請輸入1以重新輸入或輸入其他數字取消。");
						num = scanner.nextInt();
						if (num == 1){
							continue;
						}
						else {
							break;
						}
						
					} else {
						ticket = ticketable.get(ticketid);
//						System.out.println("///"+ticket.MANID);
//						System.out.println("///"+ticket.TICKETID);
						if (userid == ticket.MANID && ticketid == ticket.TICKETID) {
							System.out.println(ticket.getMANID());
							System.out.println(ticket.getTICKETID());
							System.out.println(ticket.getNAME());
							System.out.println(ticket.getTIME());
							System.out.println(ticket.getHALL());
							System.out.println(ticket.getSEAT());
							break;
						} else {
							int judge;
							System.out.println("輸入使用者ID或電影票ID錯誤，請按1重新輸入。或輸入5取消");
							judge = scanner.nextInt();
							switch (judge) {
							case 5:
								break;
							case 1:
								continue;
							}
						}
					}
				break;
				}

				break;
			case 5:
				java.util.Date i = new java.util.Date();
				int yourid, bookid, hour, minute, time;
				MOVIETICKET a;
				String choice;
				hour = i.getHours();
				minute = i.getMinutes();
				time = hour * 60 + minute;

				while (true) {
					System.out.println("請輸入你的ID：");
					yourid = scanner.nextInt();
					System.out.println("請輸入電影票ID：");
					bookid = scanner.nextInt();
					
					if (ticketable.get(bookid) == null) {
						int num;
						System.out.println("輸入電影票ID錯誤，請輸入1以重新輸入或輸入其他數字取消。");
						num = scanner.nextInt();
						if (num == 1){
							continue;
						}
						else {
							break;
						}
						
					}
					else {
						a = ticketable.get(bookid);
						
						try {
							if ((conditionfind.timetoint(a.TIME) - time > 20) || (conditionfind.timetoint(a.TIME) - time < 0)) {
								System.out.println("確定退票？(Y/N)");
								choice = scanner.next();
								switch (choice) {
								case "Y":
									String hallunbook = ticketable.get(bookid).HALL;
									String timeunbook = ticketable.get(bookid).TIME;
									switch (hallunbook){
									case "武當":
										for (int j = 0; j < woodon.length; j++) {
											if (woodon[j].time.equals(timeunbook)) {
												woodon[j].bigunbook(ticketable.get(bookid));
											}
										}
										break;
									case "少林":
										for (int j = 0; j < shoulin.length; j++) {
											if (shoulin[j].time.equals(timeunbook)) {
												shoulin[j].bigunbook(ticketable.get(bookid));
											}
										}
										break;
									case "華山":
										for (int j = 0; j < huashine.length; j++) {
											if (huashine[j].time.equals(timeunbook)) {
												huashine[j].bigunbook(ticketable.get(bookid));
											}
										}
										break;
									case "峨嵋":
										for (int j = 0; j < ermei.length; j++) {
											if (ermei[j].time.equals(timeunbook)) {
												ermei[j].smallunbook(ticketable.get(bookid));
											}
										}
										break;
									case "崆峒":
										for (int j = 0; j < hongtong.length; j++) {
											if (hongtong[j].time.equals(timeunbook)) {
												hongtong[j].smallunbook(ticketable.get(bookid));
											}
										}
									}
									ticketable.set(bookid, null);
									System.out.println("退票成功");
									break;
								case "N":
									break;
								}
							} else {
								System.out.println("由於電影將於20分鐘後開播，因此無法退票。");
							}
							break;

						} catch (MovieFuckFuckException e) {
							// TODO Auto-generated catch block
							System.out.println(e.getMessage());
							break;
						}
					}
					
				}
				

				break;
			case 6:
				System.out.println("感謝使用本訂票系統");
				System.exit(0);
				break;
			default:
				System.out.println("輸入錯誤，請重新輸入。");
				break;
			}

		}

	}

}
